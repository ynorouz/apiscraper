import requests
import json
import pandas as pd
import time
from collections import defaultdict


dataPath = '../Data/'
HEADERS = {"X-API-Key":'5b75af1dd42d4cbe87e0d545fad267fd'}
base_urld2= "https://www.bungie.net/Platform/Destiny2"

playerName = "bames123"
# playerName = "sotanahT_Reverse"
memType = "2"
ver = 2

# TODO
# In the PGCR endpoint, there is a referenceId, which is the activityHash in AggregateActivityStats
# To get a player's match history, you need to use the GetActivityHistory endpoint
# GetActivityHistory = List of their recent matches
# GetPostGameCarnageReport = A specific match
# GetDestinyAggregateActivityStats = Aggregation of those matches per activityHash


def extract_PGCR(memId, chId, memType, dataPath, fname, destinyVersion):
    # https://github.com/Bungie-net/api/issues/372

    # 1 GetActivityHistory:
    # /Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/Activities/
    if destinyVersion == 2:
        hist_url = "https://www.bungie.net/Platform/Destiny2/"+str(memType)+"/Account/"+str(memId)+"/Character/"+\
                   str(chId)+"/Stats/Activities/" #?mode=AllPvP&count=50&page=0

    elif destinyVersion == 1:
        # hist_url = "https://www.bungie.net/platform/Destiny/Stats/ActivityHistory/"+str(memType)+"/"+str(memId)+"/"+str(chId)+"/"
        # hist_url = "https://www.bungie.net/d1/Platform/Destiny/Stats/ActivityHistory/1/4611686018429681104/2305843009217260609/?mode=none"
        hist_url = "http://www.bungie.net/d1/Platform/Destiny/Stats/ActivityHistory/"+str(memType)+"/"+str(memId)+"/"+\
                   str(chId)+"/?mode=AllPvP"

    # print hist_url
    his_res = requests.get(hist_url, headers=HEADERS).json()
    # print his_res
    data = []
    # try:
        # 2 GetPostGameCarnageReport
        # /Destiny2/Stats/PostGameCarnageReport/{activityId}/
    if (his_res['ErrorCode'] == 1) and (his_res['Response']):

        if destinyVersion == 2:
            try:
                activities = his_res['Response']['activities']
            except:
                print 'No activity'
                return None

        elif destinyVersion == 1:
            try:
                activities = his_res['Response']['data']['activities']
            except:
                print 'No activity'
                return None

        for act in activities:
            print 'Look in activities for', memId, chId
            time.sleep(5)

            if destinyVersion == 2:
                hist_url = "https://www.bungie.net/Platform/Destiny2/Stats/PostGameCarnageReport/"+\
                           act['activityDetails']['instanceId']+"/"
            elif destinyVersion ==1:
                hist_url = "https://www.bungie.net/d1/platform/Destiny/Stats/PostGameCarnageReport/"+\
                           act['activityDetails']['instanceId']+"/"

            his_res = requests.get(hist_url, headers=HEADERS).json()
            # print his_res

            data.append(his_res)
            # with open(dataPath+fname+'_PGCR.json', 'w') as fp:
            #      json.dump(data, fp)

        return data
    # else:
    #     print 'Error:', his_res['ErrorStatus']


print 'Extract PvP match data for PvP players in Destiny ', ver
dataSet = []
memId_chId = pd.read_csv(dataPath+'memId_chId.csv')
memId_chId2 = pd.read_csv('../Data/PvE_PvP/PvP_perMatches_features_cluster.csv')
memId_chId3 = pd.read_csv('../Data/PvE_PvP/PvE/reinforcementLearning_data1_dailyExtraction_characterBasedJSONS_v1_accumulatedInfo_memIdchId.csv')
#
# memIds = defaultdict(list)
# for memId in memId_chId['membershipId'].unique():
#
#     # xur_url = "http://www.bungie.net/d1/Platform/Destiny/" + memType + '/Account/' + memId + "/"
#
#     #  /Destiny2/{membershipType}/Profile/{destinyMembershipId}/
#     xur_url = base_urld2 + "/" + str(memType) + "/Profile/" + str(memId) + "/"
#     print xur_url
#
#     res = requests.get(xur_url, headers=HEADERS)
#
#     # # https://github.com/Bungie-net/api/issues/147
#     # # Use the Bungie.net membership ID to find the user's linked Destiny membership IDs: GetMembershipDataById
#     # bungie_to_destiny = base_urld2 + '/User/GetMembershipsById/' +str(memType)+'/'+ str(memId)+'/'
#     # b2d = requests.get(xur_url, headers=HEADERS)
#
#     if res.json()['ErrorCode'] == 1:
#         summary = json.loads(res.content)['Response']['data']
#         characterIds = [ch['characterBase']['characterId'] for ch in summary['characters']]
#         print 'CharacterIds = ', characterIds
#
#         memIds[memId] = characterIds
#     else:
#         print  res.json()['Message']


for memId, playerProfile in memId_chId.groupby('membershipId'):

    for chId in playerProfile['characterId'].unique():
# memId='4611686018429681104'
# chId = '2305843009217260609'
# Works for D2
# memId = '4611686018436326468'
# chId = '2305843009276646017'

        # memId =4611686018431717698
        # chId=2305843009260733255
        # memId =4611686018441186705
        # chId=2305843009262483436
        # memId =4611686018436326468
        # chId=2305843009276646017
        # memId =4611686018466154572
        # chId=2305843009296008149
        # 4611686018446473450
        # 2305843009260725754
        # 4611686018437060426
        # 2305843009263164657
        # 4611686018436509051
        # 2305843009265767116
        data = extract_PGCR(memId, chId, memType, dataPath+'D2/', 'PvP_D2_matches', ver)
        if data:
            print 'data appended'
            dataSet.append(data)

with open(dataPath+'D2/PGCR_dataset.json', 'w') as fp:
   json.dump(dataSet, fp)


for memId, playerProfile in memId_chId2.groupby('membershipId'):

    for chId in playerProfile['characterId'].unique():
        data = extract_PGCR(memId, chId, memType, dataPath + 'D2/', 'PvP_D2_matches', ver)
        if data:
            print 'data appended'
            dataSet.append(data)

with open(dataPath + 'D2/PGCR_dataset2.json', 'w') as fp:
    json.dump(dataSet, fp)


for memId, playerProfile in memId_chId3.groupby('membershipId'):

    for chId in playerProfile['characterId'].unique():
        data = extract_PGCR(memId, chId, memType, dataPath + 'D2/', 'PvP_D2_matches', ver)
        if data:
            print 'data appended'
            dataSet.append(data)

with open(dataPath + 'D2/PGCR_dataset3.json', 'w') as fp:
    json.dump(dataSet, fp)


xur_url = base_urld2 + "/SearchDestinyPlayer/"+memType+"/"+playerName+"/"
resp = requests.get(xur_url, headers=HEADERS)
memId = json.loads(resp.content)['Response'][0]['membershipId']
print 'MemId', memId

print 'GetProfile'
xur_url = "http://www.bungie.net/d1/Platform/Destiny/" + memType + '/Account/' + memId + "/"
# xur_url = base_urld2 + "/" + memType + "/Profile/" + memId + "/"
res = requests.get(xur_url, headers=HEADERS)
summary = json.loads(res.content)['Response']['data']
characterIds = [ch['characterBase']['characterId'] for ch in summary['characters']]
print 'CharacterIds = ', characterIds

for chId in characterIds:
    memId = str(memId)
    chId = str(chId)

    print memId, chId

    # 1 GetActivityHistory
    hist_url = "https://www.bungie.net/Platform/Destiny2/2/Account/"+memId+"/Character/"+chId+"/Stats/Activities/"
    his_res = requests.get(hist_url, headers=HEADERS).json()
    print his_res['Response']

    # 2 GetPostGameCarnageReport
    # /Destiny2/Stats/PostGameCarnageReport/{activityId}/
    if  his_res['Response']:
        for act in his_res['Response']['activities']:
            hist_url = "https://www.bungie.net/Platform/Destiny2/Stats/PostGameCarnageReport/" + act['activityDetails'][
                'instanceId'] + "/"
            his_res = requests.get(hist_url, headers=HEADERS).json()
            print his_res['Response']

    # Find activityId to find  matches
    print 'Destiny2.GetDestinyAggregateActivityStats'
    ch_url = "https://www.bungie.net/Platform/Destiny2/" + memType + "/Account/" + memId + "/Character/" + chId + "/Stats/AggregateActivityStats/"
    ch_res = requests.get(ch_url, headers=HEADERS)
    print ch_res.json()['Response']

    print 'Destiny2.GetActivityHistory'
    # https://destinydevs.github.io/BungieNetPlatform/docs/services/Destiny2/Destiny2-GetActivityHistory

    # ch_url = base_urld2 + "/" + memType + "/Account/" + memId + "/Character/" + chId + "/Stats/Activities/"
    # Destiny2.GetHistoricalStatsForAccount
    # /Destiny2/{membershipType}/Account/{destinyMembershipId}/Stats/
    # ch_url = "https://www.bungie.net/Platform/Destiny2/2/Account/4611686018436326468/Character/2305843009276646017/Stats/AggregateActivityStats/"
    ch_url = "https://www.bungie.net/Platform/Destiny2/2/Account/4611686018436326468/Stats/"
    ch_res = requests.get(ch_url, headers=HEADERS)
    print ch_res.json()['Response']

    for act in ch_res.json()['Response']['activities']:
        url = "https://www.bungie.net/Platform/Destiny2/Stats/PostGameCarnageReport/"+ str(act['activityHash'])
        res = requests.get(url, headers=HEADERS)
        print  res.json()['Response']['data']

    # Destiny2/{platform id}/Account/{membershipId}/Character/{characterId}/Stats/?groups=1,2,3,101,102,103&modes=4,5,6,7,16,39&periodType=2

    # GetHistoricalStats: /Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/
    print 'Destiny2.GetHistoricalStats'
    chId0 = "0"  # to show account stats
    ch_url = base_urld2 + "/" + memType + "/Account/" + memId + "/Character/" + chId0 + "/Stats/?mode=5"
    ch_res = requests.get(ch_url, headers=HEADERS)
    print ch_res.json()['Response']
