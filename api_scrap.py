from lxml import html
import requests
import json
import pandas as pd
# API test
# https://destinydevs.github.io/BungieNetPlatform/docs/API-Test?code=30f08d3e2df5817b5d3305a352e37915#/Destiny2/GetHistoricalStats/p:membershipType=0,destinyMembershipId=4611686018446818061,characterId=2305843009288345584/q:periodType=0

dataPath = '../Data/'
HEADERS = {"X-API-Key":'5b75af1dd42d4cbe87e0d545fad267fd'}
base_urld1 = "https://www.bungie.net/platform/Destiny"
base_urld2= "https://www.bungie.net/Platform/Destiny2"
xur_url = "https://www.bungie.net/Platform/Destiny/Advisors/Xur/"
xur_url = "http://www.bungie.net/Platform/Destiny/2/Stats/GetMembershipIdByDisplayName/wrightflyer_1903/"
# Response': u'4611686018433385748'

# xur_url = "http://www.bungie.net/Platform/Destiny/2/Stats/GetMembershipIdByDisplayName/dci2017/"
# Response': u'4611686018463474089'

# xur_url = "https://www.bungie.net/d1/platform/Destiny/2/Account/4611686018463474089/Summary/"
# DestinyUnexpectedError

# xur_url = "https://www.bungie.net/platform/Destiny/Manifest/InventoryItem/1274330687/"
# Response': {u'data': {u'inventoryItem': {u'actionName' ....

# xur_url = "http://www.bungie.net/d1/Platform/Destiny/Manifest/"
# Response': {u'mobileAssetContentPath': ... mobileGearAssetDataBases

# Get membershipId
# 'http://www.bungie.net/Platform/Destiny/SearchDestinyPlayer/' + selectedAccountType + '/' + username
# xur_url = "http://www.bungie.net/Platform/Destiny/SearchDestinyPlayer/2/dci2017/"
# 'membershipId': u'4611686018463474089',

D = 2
playerName = "bames123"
# playerName = "sotanahT_Reverse"
memType = "2"
hashType = "6"

memId_chId = pd.DataFrame.from_csv(dataPath+'memId_chId.csv')
# TODO try Destiny2/{platform id}/Account/{membershipId}/Character/{characterId}/Stats/?groups=1,2,3,101,102,103&modes=4,5,6,7,16,39&periodType=2

if D == 2:
    # Destiny 2
    # 1) Get memId by player name
    xur_url = base_urld2 + "/SearchDestinyPlayer/"+memType+"/"+playerName+"/"

    # Send the request and store the result in res:
    print "\n\n\nConnecting to Bungie: " + xur_url + "\n"

    resp = requests.get(xur_url, headers=HEADERS)
    memId = json.loads(resp.content)['Response'][0]['membershipId']
    print 'MemId', memId

    # 2) Use memId to retriv profile(characterIds)
    # D1: xur_url = "http://www.bungie.net/Platform/Destiny/"+memType+"/Account/"+memId+"/Summary/"
    # /Destiny2/{membershipType}/Profile/{destinyMembershipId}/
    xur_url = base_urld1+ "/" + memType + "/Account/" + memId + "/Summary/"
    # xur_url =  base_urld2 + "/"+memType+"/Profile/"+memId+"/"
    res = requests.get(xur_url, headers=HEADERS)
    summary = json.loads(res.content)['Response']['data']
    characterIds = [ch['characterBase']['characterId'] for ch in summary['characters']]
    print 'CharacterIds = ', characterIds

    # with open('profileSummary.json', 'w') as outfile:
    #     json.dump(summary, outfile)

    # characterIds = ['2305843009305986974']
    # 3) character activities: Gets all activities the character has participated in together with aggregate statistics for those activities.
    # # : /Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/Activities/
    # /Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/AggregateActivityStats/
    # /Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/
    for memId, memData in memId_chId.groupby('membershipId'):

        for chId in memData['characterId'].unique():
            memId = str(memId)
            chId = str(chId)

            print memId, chId

            # Find activityId to find  matches
            print 'Destiny2.GetDestinyAggregateActivityStats'
            ch_url = "https://www.bungie.net/Platform/Destiny2/" + memType+"/Account/"+memId+"/Character/"+chId+"/Stats/AggregateActivityStats/"
            # ch_url = base_urld2 + "/Stats/AggregateActivityStats/" + memType + "/" + memId + "/" + chId + "/"
            ch_res = requests.get(ch_url, headers=HEADERS)
            print ch_res.json()['Response']

            print 'Destiny2.GetActivityHistory'
            ch_url = base_urld2 + "/" + memType + "/Account/" + memId + "/Character/" + chId + "/Stats/Activities/"
            ch_res = requests.get(ch_url, headers=HEADERS)
            print ch_res.json()['Response']

            # Destiny2/{platform id}/Account/{membershipId}/Character/{characterId}/Stats/?groups=1,2,3,101,102,103&modes=4,5,6,7,16,39&periodType=2

            # GetHistoricalStats: /Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/
            print 'Destiny2.GetHistoricalStats'
            chId0 = "0" # to show account stats
            ch_url = base_urld2 + "/" + memType + "/Account/" + memId + "/Character/" + chId0+ "/Stats/?mode=5"
            ch_res = requests.get(ch_url, headers=HEADERS)
            print ch_res.json()['Response']

            print 'AggregateActivityStats in D1'
            ch_url = base_urld1 + "/Stats/AggregateActivityStats/" + memType + "/" + memId + "/" + chId + "/"
            ch_res = requests.get(ch_url, headers=HEADERS)
            print ch_res.json()['Response']['data']['activities']

            # for act in ch_res.json()['Response']['data']['activities']:
            #     #  /Destiny2/Stats/PostGameCarnageReport/{activityId}/
            #     url = base_urld2+"/Stats/PostGameCarnageReport/"+ str(act['activityHash'])
            #     res = requests.get(url, headers=HEADERS)
            #     print  res.json()['Response']['data']

# TODO AggregateActivityStats works with D1, but not D2.
# "/Stats/AggregateActivityStats/{membershipType}/{destinyMembershipId}/{characterId}/"
for chId in characterIds:
    ch_url = base_urld1+"/Stats/AggregateActivityStats/"+memType+"/"+memId+"/"+chId+"/"

    ch_res = requests.get(ch_url, headers=HEADERS)
    print ch_res.json()['Response']['data']['activities']

    for act in ch_res.json()['Response']['data']['activities']:
        url = base_urld1+"/Stats/PostGameCarnageReport/"+ str(act['activityHash'])
        res = requests.get(url, headers=HEADERS)
        print  res.json()['Response']['data']
    #
    act_url = base_urld1+"/Stats/ActivityHistory/"+memType+"/"+memId+"/"+chId+"/"
    act_res = requests.get(act_url, headers=HEADERS)
    print ch_res.json()

    # /Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/Activities/
    act_stat_url = "http://www.bungie.net/Platform/Destiny2/"+memType+"/Account/"+memId+"/Character/"+chId+"/Stats/Activities/"
    act_stat_res = requests.get(act_url, headers=HEADERS)
    print act_stat_res.json()['Response']['data']
# 4) matches of a character
'https://www.bungie.net/Platform/Destiny/Stats/ActivityHistory/2/4611686018428830098/2305843009214965975/?mode=FreeForAll&count=250'

# /Stats/ActivityHistory/2/4611686018428388123/2305843009215655427/?mode=Story

# Print the error status:
error_stat = res.json()['ErrorStatus']
print "Error status: " + error_stat + "\n"

# Uncomment this line to print JSON output to a file:
# f.write(json.dumps(res.json(), indent=4))

print "##################################################"
print "## Printing Xur's inventory:"
print "##################################################"

for saleItem in res.json()['Response']['data']['saleItemCategories']:
    mysaleItems = saleItem['saleItems']
    for myItem in mysaleItems:
        hashID = str(myItem['item']['itemHash'])
        hashReqString = base_url + "Manifest/" + hashType + "/" + hashID
        res = requests.get(hashReqString, headers=HEADERS)
        item_name = res.json()['Response']['data']['inventoryItem']['itemName']
        print "Item is: " + item_name
        item_type = res.json()['Response']['data']['inventoryItem']['itemTypeName']
        item_tier = res.json()['Response']['data']['inventoryItem']['tierTypeName']
        print "Item type is: " + item_tier + " " + item_type + "\n"
