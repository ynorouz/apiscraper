import requests
import json
import pandas as pd
import time
import numpy as np
from collections import defaultdict


dataPath = '../Data/'
HEADERS = {"X-API-Key":'5b75af1dd42d4cbe87e0d545fad267fd'}
base_urld2= "https://www.bungie.net/Platform/Destiny2"
memType = "2"


def extract_memId_chId(json_line):
    """
    Extract player data from matches data
    :param json_line: one line of json
    :return:
    """
    memId_chIds = defaultdict(set)
    # for line in json_data:
    #     if line:
    for act in json_line:
        try:
            # for player in act['Response']['data']['entries']:
            for player in act['Response']['entries']:
                memId = player['player']['destinyUserInfo']['membershipId']
                memId_chIds[memId] .add(player['characterId'])
        except:
            pass
    return memId_chIds



def extract_PGCR(memId, chId, memType, dataPath, fname, destinyVersion):
    # https://github.com/Bungie-net/api/issues/372

    # 1 GetActivityHistory:
    # /Destiny2/{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Stats/Activities/
    if destinyVersion == 2:
        hist_url = "https://www.bungie.net/Platform/Destiny2/"+str(memType)+"/Account/"+str(memId)+"/Character/"+\
                   str(chId)+"/Stats/Activities/" #?mode=AllPvP&count=50&page=0

    elif destinyVersion == 1:
        # hist_url = "https://www.bungie.net/platform/Destiny/Stats/ActivityHistory/"+str(memType)+"/"+str(memId)+"/"+str(chId)+"/"
        # hist_url = "https://www.bungie.net/d1/Platform/Destiny/Stats/ActivityHistory/1/4611686018429681104/2305843009217260609/?mode=none"
        hist_url = "http://www.bungie.net/d1/Platform/Destiny/Stats/ActivityHistory/"+str(memType)+"/"+str(memId)+"/"+\
                   str(chId)+"/?mode=AllPvP"

    # print hist_url
    try:
        his_res = requests.get(hist_url, headers=HEADERS).json()
    except:
        return []
    # print his_res
    data = []
    # try:
        # 2 GetPostGameCarnageReport
        # /Destiny2/Stats/PostGameCarnageReport/{activityId}/
    if (his_res['ErrorCode'] == 1) and (his_res['Response']):

        if destinyVersion == 2:
            try:
                activities = his_res['Response']['activities']
            except:
                print 'No activity'
                return None

        elif destinyVersion == 1:
            try:
                activities = his_res['Response']['data']['activities']
            except:
                print 'No activity'
                return None

        for act in activities:
            # The general rate limit is 25 requests per second averaged over 10 seconds.
            # This does not apply to inventory calls like moving an item.  Those are supposed to be done sequentially
            # (as in wait until the first one completes before sending the next).  There are some other complexities,
            # but that is the 10,000 ft view.  If you are going to be sending a lot of requests I would be watching all
            # of the base fields like ErrorCode, ThrottleSeconds, etc.  They will tell you if you are being limited.
            # And I believe the limit is by IP address so if you have multiple processes going they all need to stay
            # under that in total
            time.sleep(1)

            if destinyVersion == 2:
                hist_url = "https://www.bungie.net/Platform/Destiny2/Stats/PostGameCarnageReport/"+\
                           act['activityDetails']['instanceId']+"/"
            elif destinyVersion ==1:
                hist_url = "https://www.bungie.net/d1/platform/Destiny/Stats/PostGameCarnageReport/"+\
                           act['activityDetails']['instanceId']+"/"
            try:
                his_res = requests.get(hist_url, headers=HEADERS).json()
                # print his_res

                data.append(his_res)
            # with open(dataPath+fname+'_PGCR.json', 'w') as fp:
            #      json.dump(data, fp)
            except:
                pass
        return data
    # else:
    #     print 'Error:', his_res['ErrorStatus']


def get_chIds(memId, memType, HEADERS, DestinyVersion, base_urld2):

    characterIds = []
    # print 'GetProfile'
    if DestinyVersion == 1:
        xur_url = "http://www.bungie.net/d1/Platform/Destiny/" + memType + '/Account/' + memId + "/"
    elif DestinyVersion == 2:
        xur_url = base_urld2 + "/" + memType + "/Profile/" + memId + "/?components=100"

    res = requests.get(xur_url, headers=HEADERS)

    if DestinyVersion == 1:
        summary = json.loads(res.content)['Response']['data']
        characterIds = [ch['characterBase']['characterId'] for ch in summary['characters']]
    elif DestinyVersion == 2:
        try:
            characterIds = set(res.json()['Response']['profile']['data']['characterIds'])
        except:
            pass

    return characterIds

def json_to_dict(dataPath, fname):
    # Extract membershipIds for the first set of ids
    memId_chIds = defaultdict(set)
    # with open(dataPath+'PGCR_dataset.json') as fp:
    with open(dataPath+fname+'.json') as fp:
        json_data = json.load(fp)

        for line in json_data:
            if line:
                memId_chIds.update(extract_memId_chId(line))

    return memId_chIds

def dict_to_csv(memId_chIds, dataPath, fname):
    # Dict to DF
    memId_chIds_df = pd.DataFrame.from_dict(memId_chIds, orient='index')
    memId_chIds_df['membershipId'] = memId_chIds_df.index
    memId_chIds_df = memId_chIds_df.rename(columns={0: "character1", 1: "character2", 2: "character3", 3:"parsed"})
    memId_chIds_df.drop([4], axis=1, inplace=True)
    memId_chIds_df['parsed'] = 0
    memId_chIds_df.to_csv(dataPath + fname + '.csv')

# # Read json to find Ids
# memId_chIds= json_to_dict(dataPath+'D2/', "PGCR_graph_dataset0")

# # Write dictionary of Ids to csv
# dict_to_csv(memId_chIds, dataPath+'D2/', 'meId_chIds_base')

# Read membershipIds for the second set of ids
# memId_chIds_df = pd.read_csv(dataPath+'D2/meId_chIds_base.csv', dtype={'membershipId':str, 'character1':str,
#                                                                        'character2':str, 'character3':str,
#                                                                        'parsed': np.uint16})
# memId_chIds_df = pd.read_csv(dataPath+'D2/meId_chIds12.csv', dtype={'membershipId':str, 'character1':str,
#                                                                    'character2':str, 'character3':str,
#                                                                    'parsed': np.uint16})
# Select characters that are not parsed
# memId_chIds_df = memId_chIds_df[memId_chIds_df['parsed'] == 0]
# memId_chIds_df = memId_chIds_df.dropna(axis=0, how='all')

# Read 100k membershipId given by Bungie
memId_chIds_df = pd.read_csv(dataPath+'D2/ID_Pull.csv')
memId_chIds_df.columns = ['membershipId', 'memType']
memId_chIds_df[['membershipId', 'memType']] = memId_chIds_df[['membershipId', 'memType']].astype('str')
memId_chIds_df['parsed'] = 0

# Extract match activities for all characters of a given player
dataSet = []
dataSetIter = 1
for memId in memId_chIds_df['membershipId'].unique():

    # Get all characterIds for this membershipId
    chIds = get_chIds(memId, memId_chIds_df[memId_chIds_df['membershipId']==memId].iloc[0]['memType'], HEADERS, 2, base_urld2)

    # # If get_chIds() can not finds chIds, use chIds that exist in  memId_chIds
    # if len(chIds) == 0:
    #     chIds = memId_chIds[memId]
    #
    # # Update dict
    # memId_chIds[memId] = chIds

    # Update DF
    # memId_chIds_df.append([memId]+chIds)
    # memId_chIds_df[memId_chIds_df['membershipId'] == memId]['parsed'] = 0
    mask = memId_chIds_df['membershipId'] == memId
    chIds_len = 0

    for chId in list(chIds):
        chIds_len += 1
        memId_chIds_df.loc[mask, 'character'+str(dataSetIter)] = chIds.pop()

        json_data = extract_PGCR(memId, chId, memType, dataPath, fname='PGCR_graph_dataset', destinyVersion=2)

        if json_data:
            # print 'data appended'
            dataSet.append(json_data)

            # find new players
            # new_memId_chIds = extract_memId_chId(json_data)
            # memId_chIds.update(new_memId_chIds)

            # Numbre of characters that parsed for this player
            memId_chIds_df.loc[mask, 'parsed'] += 1

    # Write each 1000 samples to a new json file
    if len(dataSet) > 1000:
        print 'Write to JSON dataset ...'
        with open(dataPath+'D2/PGCR_graph_dataset'+str(dataSetIter)+'.json', 'w') as fp:
           json.dump(dataSet, fp)

        memId_chIds_df.to_csv(dataPath+'D2/meId_chIds' + str(dataSetIter)+'.csv')

        dataSetIter += 1
        dataSet = []